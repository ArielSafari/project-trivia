#pragma once

#include "Helper.h"

#include "Room.h"
#include "Game.h"

class Room;
class Game;

class User
{
private:

	string _username;
	Room* _currRoom;
	Game* _currGame;
	SOCKET _sock;

public:

	User(string, SOCKET);
	void send(string);
	
	string getUsername();
	SOCKET getSocket();
	Room* getRoom();
	Game* getGame();
	void setGame(Game*);

	void clearRoom();
	bool createRoom(int, string, int, int, int);
	bool joinRoom(Room*);
	void leaveRoom();
	int closeRoom();
	bool leaveGame();

};

#pragma once


class Protocol
{
public:

	/********************
	 * CLIENT -> SERVER: 2xx
	 * SERVER -> CLIENT: 1xx
	*************/

	// sign options
	static const int RQST_SIGNIN = 200;
	static const int RQST_SIGNOUT = 201;
	static const int ANSWER_SIGNIN = 102;

	static const int RQST_SIGNUP = 203;
	static const int ANSWER_SIGNUP = 104;

	// rooms list
	static const int RQST_ROOMS_LIST = 205;
	static const int ANSWER_ROOMS_LIST = 106;

	// users in room list
	static const int RQST_USERS_IN_ROOM = 207;
	static const int ANSWER_USERS_IN_ROOM = 108;

	// join \ leave room
	static const int RQST_JOIN_ROOM = 209;
	static const int ANSWER_JOIN_ROOM = 110;

	static const int RQST_LEAVE_ROOM = 211;
	static const int ANSWER_LEAVE_ROOM = 112;

	// create \ close room
	static const int RQST_CREATE_ROOM = 213;
	static const int ANSWER_CREATE_ROOM = 114;

	static const int RQST_CLOSE_ROOM = 215;
	static const int ANSWER_CLOSE_ROOM = 116;

	// GAME START
	static const int RQST_START_GAME = 217;

	// questions
	static const int SEND_QUESTION = 118;
	static const int RCV_ANSWER = 219;
	static const int SEND_QUESTION_FEEDBACK = 120;

	// GAME END
	static const int SEND_GAME_ENDED = 121;

	static const int RQST_LEAVE_GAME = 222;
	
	// scores (my\all)
	static const int RQST_BEST_SCORES = 223;
	static const int ANSWER_BEST_SCORES = 124;

	static const int RQST_MY_SCORES = 225;
	static const int ANSWER_MY_SCORES = 126;

	// EXIT APPLICATION
	static const int RQST_EXIT = 299;

};
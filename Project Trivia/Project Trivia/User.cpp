#include "User.h"


User::User(string username, SOCKET sock)
{
	_username = username;
	_sock = sock;

	_currRoom = nullptr;
	_currGame = nullptr;
}

void User::send(string)
{
	throw NotImplementedException();
}

string User::getUsername()
{
	return _username;
}

SOCKET User::getSocket()
{
	return _sock;
}

Room* User::getRoom()
{
	return _currRoom;
}

Game* User::getGame()
{
	return _currGame;
}

void User::setGame(Game* newGame)
{
	_currGame = newGame;
}

void User::clearRoom()
{
	_currRoom = nullptr;
}

bool User::createRoom(int, string, int, int, int)
{
	throw NotImplementedException();
}

bool User::joinRoom(Room*)
{
	throw NotImplementedException();
}

void User::leaveRoom()
{
	throw NotImplementedException();
}

int User::closeRoom()
{
	throw NotImplementedException();
}

bool User::leaveGame()
{
	throw NotImplementedException();
}

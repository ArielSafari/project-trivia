#pragma once

#include "Helper.h"

class Question
{

private:

	string _question;
	string _answers[4];
	int _correctAnswerIndex;
	int _id;

private:

	Question(int, string, string, string, string, string);

	string getQuestion();
	string* getAnswers();
	int getCorrectAnswerIndex();
	int getId();

};
#include "TriviaServer.h"

// makes the db
// creates new socket (if not - throws exception)
TriviaServer::TriviaServer()
{
	_roomIdSequence = 0;

	// create new socket


}

// deletes the rooms list
// deletes the connected users
// closes all opened socket
TriviaServer::~TriviaServer()
{
	// delete the rooms list
	std::map<int, Room*>::iterator itRooms = _roomsList.begin();
	for (; itRooms != _roomsList.end(); ++itRooms)
		delete itRooms->second;

	// delete the conected users list
	std::map<SOCKET, User*>::iterator itUsers = _connectedUsers.begin();
	for (; itUsers != _connectedUsers.end(); ++itUsers)
	{
		closesocket(itUsers->first); // close the users sockets
		delete itUsers->second;
	}

	// close the main socket
	closesocket(_socket);
}

void TriviaServer::server()
{
	
}

////////////////////////////////
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

void TriviaServer::bindAndListen()
{
	throw NotImplementedException();
}

void TriviaServer::accept()
{
	throw NotImplementedException();
}

void TriviaServer::clientHandler(SOCKET sock)
{
	throw NotImplementedException();
}

void TriviaServer::safeDeleteUser(RecievedMessage* msg)
{
	throw NotImplementedException();
}

////////////////////////////////
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

User* TriviaServer::handleSignin(RecievedMessage* msg)
{
	throw NotImplementedException();
}

bool TriviaServer::handleSignup(RecievedMessage* msg)
{
	throw NotImplementedException();
}

void TriviaServer::handleSignout(RecievedMessage* msg)
{
	throw NotImplementedException();
}

////////////////////////////////
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

void TriviaServer::handleLeaveGame(RecievedMessage* msg)
{
	throw NotImplementedException();
}

void TriviaServer::handleStartGame(RecievedMessage* msg)
{
	throw NotImplementedException();
}

void TriviaServer::handlePlayerAnswer(RecievedMessage* msg)
{
	throw NotImplementedException();
}

////////////////////////////////
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

// handle the "create room" request (code: 213)
// @returns TRUE if succeded, FALSE otherwise
bool TriviaServer::handleCreateRoom(RecievedMessage* msg)
{
	// get the related user for the socket
	msg->setUser(getUserBySocket(msg->getSock()));

	User* userPtr = msg->getUser(); // just to call getUser() only once

	if (userPtr) // if: user exsists
	{
		if (!(msg->getValues().empty())) // if: msg has values
		{
			_roomIdSequence++;

			// call "create room" on the user
			int roomNameSize;
			string roomName;
			int playersNum;
			int questionsNum;
			int questionTimeInSec;

			bool retVal = userPtr->createRoom(roomNameSize, roomName,
				playersNum, questionsNum, questionTimeInSec);

			if (retVal) // function "createRoom" succeded
			{
				// add the room to the rooms list
				userPtr->getRoom();
				_roomsList.insert (
					pair<int, Room*> (
						userPtr->getRoom()->getId(), userPtr->getRoom()
					));
			}
		}
	}

	return false;
}

// handle the "close room" request (code: 215)
// @returns TRUE if succeded, FALSE otherwise
bool TriviaServer::handleCloseRoom(RecievedMessage* msg)
{
	// get the related user for the socket
	msg->setUser(getUserBySocket(msg->getSock()));

	User* userPtr = msg->getUser(); // just to call getUser() only once

	if (userPtr) // if: user exsists
	{
		// get users room
		Room* roomPtr = userPtr->getRoom();

		if (roomPtr) // if: room exsists
		{
			// call "close room" on the user			
			if (userPtr->closeRoom() != -1)
			{
				// delete the room from the rooms list
				_roomsList.erase(userPtr->getRoom()->getId());

				return true;
			}
		}
	}

	return false;

}

// handle the "join room" request (code: 209)
// @returns TRUE if succeded, FALSE otherwise
// msg: [209roomID]
bool TriviaServer::handleJoinRoom(RecievedMessage* msg)
{
	// get the related user for the socket
	msg->setUser(getUserBySocket(msg->getSock()));

	User* userPtr = msg->getUser(); // just to call getUser() only once

	if (userPtr) // if: user exsists
	{
		int roomId = Helper::getIntPartFromSocket(msg->getSock(), 4);
		Room* roomPtr = getRoomById(roomId);

		if (roomPtr) // if: room exsists
		{
			// join the user to the room
			return userPtr->joinRoom(roomPtr);
		}
		else // failed - room not exist or other reason [1102]
		{
			// send failure message to the user
			Helper::sendData(userPtr->getSocket(), "[1102]");
		}
	}

	return false;
}

// handle the "leave room" request (code: 211)
// @returns TRUE if succeded, FALSE otherwise
bool TriviaServer::handleLeaveRoom(RecievedMessage* msg)
{
	// get the related user for the socket
	msg->setUser(getUserBySocket(msg->getSock()));
	
	User* userPtr = msg->getUser(); // just to call getUser() only once

	if (userPtr) // if: user exsists
	{
		// get the related room for the user
		if (userPtr->getRoom()) // if: user in a room
		{
			// activate User.leaveRoom()
			userPtr->leaveRoom();
			return true;
		}
	}

	return false;

}

void TriviaServer::handleGetUsersInRoom(RecievedMessage* msg)
{
	throw NotImplementedException();
}

void TriviaServer::handleGetRooms(RecievedMessage* msg)
{
	throw NotImplementedException();
}

////////////////////////////////
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

void TriviaServer::handleGetBestScores(RecievedMessage* msg)
{
	throw NotImplementedException();
}

void TriviaServer::handleGetPersonalStatus(RecievedMessage* msg)
{
	throw NotImplementedException();
}

////////////////////////////////
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

void TriviaServer::handleRecievedMessages()
{
	throw NotImplementedException();
}

void TriviaServer::addRecievedMessage(RecievedMessage* msg)
{
	throw NotImplementedException();
}

RecievedMessage* TriviaServer::buildRecieveMessage(SOCKET, int)
{
	throw NotImplementedException();
}

////////////////////////////////
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

// seeks for the user in the connected users list by his NAME
// @returns User* OR nullptr
User* TriviaServer::getUserByName(string name)
{
	map<SOCKET, User*>::iterator it = _connectedUsers.begin();

	for (; it != _connectedUsers.end(); it++)
		if (it->second->getUsername() == name)
			return it->second; // if found -> return the user ptr

	return nullptr; // if not found
}

// seeks for the user in the connected users list by his SOCKET
// @returns User* OR nullptr
User* TriviaServer::getUserBySocket(SOCKET sock)
{
	map<SOCKET, User*>::iterator it = _connectedUsers.find(sock);

	if (it != _connectedUsers.end()) // user found
		return it->second; // return the user ptr

	return nullptr; // if not found
}

// seeks for the room by his id
// @returns Room* OR nullptr
Room* TriviaServer::getRoomById(int id)
{
	map<int, Room*>::iterator it = _roomsList.find(id);

	if (it != _roomsList.end()) // room found
		return it->second; // return the room ptr

	return nullptr; // if not found
}


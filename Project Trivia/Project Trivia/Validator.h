#pragma once

#include "Helper.h"

using namespace std;


class Validator
{

public:

	static bool isPasswordValid(string);
	static bool isUsernameValid(string);

};
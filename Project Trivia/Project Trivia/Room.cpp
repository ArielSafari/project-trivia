#include "Room.h"
Room::Room(int id, User* Admin, string name, int maxUsers, int questionNo, int questionTime)
{
	_id = id;
	_admin = Admin;
	vec.insert(vec.begin(), Admin);
	_name = name;
	_qustionNo = questionNo;
	_maxUsers = maxUsers;
	_questionTime = questionTime;
}
string Room::getUsersListMessage()
{
	int i;
	string list="";
	User* u;
	for (i = 0; i < vec.size(); i++)
	{
		u = vec[i];
		list.append(u.getName());
	}
	return list;
}
//needs change - show message to the users
//needs change - shoe messege to the user 
void Room::leaveRoom(User* user)
{
	int i, index = -1;
	for (i = 0; i < vec.size(); i++)
	{
		if (user == vec[i])
		{
			index = i;
		}
	}
	if (index != -1)
	{
		vec.erase(vec.begin() + index);
	}
}
//need change - show messege to all users that the room is being closed
int Room::closeRoom(User* user)
{
	User* u;
	int i;
	if (user == _admin)
	{
		for (i = 0; i < vec.size(); i++)
		{
			u = vec[i];
			u.clearRoom();
		}
	}
	else
		return -1;
}

int Room::getId()
{
	return _id;
}